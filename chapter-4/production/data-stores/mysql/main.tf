provider "aws" {
  region = "us-east-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.48"
}

terraform {
  backend "s3" {

    # Note: backends can't use VARS.
    # use a backend.hcl file

    # This backend configuration is filled in automatically at test time by Terratest. If you wish to run this example
    # manually, uncomment and fill in the config below.

    # bucket         = "<YOUR S3 BUCKET>"
    # key            = "<SOME PATH>/terraform.tfstate"
    # region         = "us-east-1"
    # dynamodb_table = "<YOUR DYNAMODB TABLE>"
    # encrypt        = true

    key = "production/data-stores/mysql/terraform.tfstate"
  }
}

module "data_stores_mysql" {
  source = "../../../modules/data-stores/mysql"

  db_name          = var.db_name
  db_password      = var.db_password
  db_instance_type = "db.t2.medium"
}
